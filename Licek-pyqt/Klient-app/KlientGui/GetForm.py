# -*- coding: utf-8 -*-
import ConnectAndSend
import Frame
from package import Serialization


class GetForm(Frame.QtGui.QWizardPage):
    def __init__(self, parent=None):
        super(GetForm, self).__init__(parent)

        self.key = ""
        self.keylist = []


        self.answer = ""
        self.serializationObject = Serialization.Serialization()

        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setText(Frame.QtCore.QString.fromUtf8("Pobierz formularz"))
        self.l1.setFont(Frame.QtGui.QFont("Times", 36))
        self.l1.move(320, 20)

        self.l2 = Frame.QtGui.QLabel(self)
        self.l2.setText(Frame.QtCore.QString.fromUtf8('''
W miejsce poniżej proszę przepisać klucz. Jeśli formularz jest już gotowy zostanie pobrany,
w innym przypadku proszę poczekać, aż druga ze stron prześle dane.'''))
        self.l2.setFont(Frame.QtGui.QFont("Times", 20))
        self.l2.move(20, 100)

        self.textBox = Frame.QtGui.QLineEdit(self)
        self.textBox.move(350, 400)
        self.textBox.resize(300, 50)

        self.pobierzForm = Frame.QtGui.QPushButton(self)
        self.pobierzForm.connect(self.pobierzForm, Frame.QtCore.SIGNAL("clicked()"), self.getForm )
        self.pobierzForm.setText("Pobierz formularz")
        self.pobierzForm.move(420, 470)


    def getTextBoxValue(self):
        a = self.textBox.text()
        return str(a)

    def getForm(self):
        regquest = self.getTextBoxValue()
        self.key = regquest
        print regquest
        ans = ConnectAndSend.ConnectAndSend()
        self.get_key()
        self.answer= ans.sendRequest(self.keylist[0])

        if self.serializationObject.is_pickle(self.answer):
            Frame.Frame.SLOgetedform = self.serializationObject.un_pack(self.answer)
            Frame.Frame.readyObject.kluczsprawy = Frame.Frame.SLOgetedform.kluczsprawy
            Frame.Frame.readyObject.strona = self.keylist[3]
            self.showMessage("Formularz gotowy, przejdź dalej!")





        else:
            self.showMessage(self.answer)


    def showMessage(self, answer):
        self.msg = Frame.QtGui.QMessageBox(self)
        self.msg.setIcon(Frame.QtGui.QMessageBox.Information)
        self.msg.setStandardButtons(Frame.QtGui.QMessageBox.Ok)
        self.msg.setText(Frame.QtCore.QString.fromUtf8(answer))
        self.result = self.msg.exec_()




    def get_key(self):
        self.keylist = self.key.split('-')
        print self.keylist




    def nextId(self):
        return Frame.Frame.Kategoria_1