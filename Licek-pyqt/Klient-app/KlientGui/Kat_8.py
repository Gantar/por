# -*- coding: utf-8 -*-
import Frame


class Kat_8(Frame.QtGui.QWizardPage):

    def __init__(self, parent= None):
        super(Kat_8, self).__init__(parent)
        self.flaga = 1


        self.updatedPoints = Frame.Frame.pulaPunktow



        self.Kat_8_GuiList = []
        self.layoutA = Frame.QtGui.QVBoxLayout(self)

        self.HBox1 = Frame.QtGui.QHBoxLayout()
        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setText(Frame.QtCore.QString.fromUtf8("8. Informowanie i dokumentacja"))
        self.l1.setFont(Frame.QtGui.QFont("Times", 20))
        self.HBox1.addWidget(self.l1)


        self.udateGuiButton = Frame.QtGui.QPushButton(self)
        self.udateGuiButton.setText("Aktualizuj Gui")
        self.udateGuiButton.setFixedSize(100, 30)
        self.udateGuiButton.connect(self.udateGuiButton, Frame.QtCore.SIGNAL("clicked()"), self.update_Gui)
        self.udateGuiButton.resize(15,50)
        self.HBox1.addWidget(self.udateGuiButton)

        self.pointsPoolLabel = Frame.QtGui.QLineEdit(self)
        self.pointsPoolLabel.setEnabled(False)
        self.pointsPoolLabel.setFixedSize(100,30)
        self.pointsPoolLabel.setText(str(Frame.Frame.pulaPunktow))
        self.HBox1.addWidget(self.pointsPoolLabel)

        self.layoutA.addLayout(self.HBox1)

        self.formLayout = Frame.QtGui.QGridLayout()
        self.scrollWidget = Frame.QtGui.QWidget()
        self.scrollWidget.setLayout(self.formLayout)

        # scroll area
        self.scrollArea = Frame.QtGui.QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollWidget)

        # main layout
        self.mainLayout = Frame.QtGui.QVBoxLayout()

        # add all main to the main vLayout
        self.mainLayout.addWidget(self.scrollArea)


        self.layoutA.addLayout(self.mainLayout)

        self.HBox2 = Frame.QtGui.QHBoxLayout()


        self.clearButton = Frame.QtGui.QPushButton(self)
        self.clearButton.setText(Frame.QtCore.QString.fromUtf8("Czyść"))
        self.clearButton.connect(self.clearButton, Frame.QtCore.SIGNAL("clicked()"), self.czysc)
        self.HBox2.addWidget(self.clearButton)

        self.countButton = Frame.QtGui.QPushButton(self)
        self.countButton.setText(Frame.QtCore.QString.fromUtf8("Podlicz/Sprawdź"))
        self.countButton.connect(self.countButton, Frame.QtCore.SIGNAL("clicked()"), self.podlicz)
        self.HBox2.addWidget(self.countButton)

        self.registerButton = Frame.QtGui.QPushButton(self)

        self.registerButton.setText("Akceptuj")
        self.registerButton.connect(self.registerButton, Frame.QtCore.SIGNAL("clicked()"), self.getVal)
        self.HBox2.addWidget(self.registerButton)

        self.layoutA.addLayout(self.HBox2)


        self.setLayout(self.layoutA)




    def showMessage(self, answer):
        self.msg = Frame.QtGui.QMessageBox(self)
        self.msg.setIcon(Frame.QtGui.QMessageBox.Critical)
        self.msg.setStandardButtons(Frame.QtGui.QMessageBox.Close)
        self.msg.setText(Frame.QtCore.QString.fromUtf8(answer))
        self.result = self.msg.exec_()



    def podlicz(self):
        self.updatedPoints = Frame.Frame.pulaPunktow
        for i in range(len(self.formLayout)):
            hbox = self.formLayout.itemAt(i)
            self.updatedPoints = self.updatedPoints - int(hbox.itemAt(1).widget().text())
        if self.updatedPoints<0:
            self.showMessage("Nie możesz przekroczyć 0 !!!")
            self.registerButton.setEnabled(False)
            self.czysc()
            return False
        else:
            self.registerButton.setEnabled(True)
            hbox = self.layoutA.itemAt(0)
            hbox.itemAt(2).widget().setText(str(self.updatedPoints))
            return True





    def czysc(self):
        for i in range(len(self.formLayout)):
            hbox = self.formLayout.itemAt(i)
            hbox.itemAt(1).widget().setText("0")
            self.podlicz()


    def update_Gui(self):
        a = 0
        self.flaga = 0
        for i in Frame.Frame.SLOgetedform.Kat_8_Option:
            print i
            self.HBox3 = Frame.QtGui.QHBoxLayout()
            self.HBox3.setSpacing(0)
            self.HBox3.setMargin(0)

            label = Frame.QtGui.QLabel(self)
            label.setText(Frame.QtCore.QString.fromUtf8(i))
            label.setFont(Frame.QtGui.QFont("Times", 20))

            qline = Frame.QtGui.QLineEdit(self)
            qline.setFixedSize(100, 30)
            qline.setText("0")

            self.HBox3.addWidget(label, 0)
            self.HBox3.addWidget(qline, 0)

            self.pointsPoolLabel.setText(str(Frame.Frame.pulaPunktow))


            self.formLayout.addLayout(self.HBox3, a, 0)
            a = a + 1




    def getVal(self):
        if self.podlicz():
            self.flaga = 1
            Frame.Frame.pulaPunktow = self.updatedPoints
            for i in range(len(self.formLayout)):
               hbox = self.formLayout.itemAt(i)
               Frame.Frame.readyObject.Kat_8_Option.append(int(hbox.itemAt(1).widget().text()))
            self.countButton.setEnabled(False)
            self.clearButton.setEnabled(False)
            self.registerButton.setEnabled(False)
            for i in range(len(self.formLayout)):
                hbox = self.formLayout.itemAt(i)
                hbox.itemAt(1).widget().setEnabled(False)
            self.udateGuiButton.setEnabled(False)
        else:
            self.czysc()





    def nextId(self):
        if self.flaga ==0:
            self.showMessage("Aby przejść dalej musisz zaakceptować zmiany !")
        else:
            return Frame.Frame.Kategoria_9