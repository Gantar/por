# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore

import ConnectToServer
import DodawanieKategorii
import EvaluatePage
import GetForm
import IntroPage
import Kat_1
import Kat_10
import Kat_2
import Kat_3
import Kat_4
import Kat_5
import Kat_6
import Kat_7
import Kat_8
import Kat_9
import LastPage
import RegisterPage
from package import ServerList


class Frame(QtGui.QWizard, ServerList.ServerList):
    NUM_PAGES = 17
    serverListObject = ServerList.ServerList()
    SLOgetedform = ServerList.ServerList()
    readyObject = ServerList.ServerList()
    pulaPunktow = 100
    yesback = [5]


    ##NextGuiList
    Kat_1_Gui_List = []
    licznika = 30

    (PageIntro, PageEvaluate, PageRegister, DodajKategorie,
     PolaczZSerwerem, WyslijKlucz, Kategoria_1, Kategoria_2,
     Kategoria_3, Kategoria_4, Kategoria_5, Kategoria_6,
     Kategoria_7, Kategoria_8, Kategoria_9, Kategoria_10, StronaOstatnia) = range(NUM_PAGES)

    def __init__(self, parent = None):
        super(Frame, self).__init__(parent)
        self.setGeometry(50, 50, 1024, 768)
        self.setWindowTitle("Nazwa okna")
        self.setFixedSize(self.size())
        QtGui.QWizard.setButtonText(self, QtGui.QWizard.CancelButton, "Anuluj")
        QtGui.QWizard.setButtonText(self, QtGui.QWizard.FinishButton, "Koniec")
        QtGui.QWizard.setButtonText(self, QtGui.QWizard.BackButton, QtCore.QString.fromUtf8("Powrót"))

        self.noback = [5, 6, 16]

        self.currentIdChanged.connect(self.disable_back)







        QtGui.QWizard.setButtonText(self, QtGui.QWizard.NextButton, "Dalej")
        self.setPage(self.PageIntro, IntroPage.IntroPage(self))
        self.setPage(self.PageEvaluate, EvaluatePage.EvaluatePage())
        self.setPage(self.PageRegister, RegisterPage.RegisterPage())
        self.setPage(self.DodajKategorie, DodawanieKategorii.DodawanieKategorii())
        self.setPage(self.PolaczZSerwerem, ConnectToServer.ConnectToServer())
        self.setPage(self.WyslijKlucz, GetForm.GetForm())
        self.setPage(self.Kategoria_1, Kat_1.Kat_1())
        self.setPage(self.Kategoria_2, Kat_2.Kat_2())
        self.setPage(self.Kategoria_3, Kat_3.Kat_3())
        self.setPage(self.Kategoria_4, Kat_4.Kat_4())
        self.setPage(self.Kategoria_5, Kat_5.Kat_5())
        self.setPage(self.Kategoria_6, Kat_6.Kat_6())
        self.setPage(self.Kategoria_7, Kat_7.Kat_7())
        self.setPage(self.Kategoria_8, Kat_8.Kat_8())
        self.setPage(self.Kategoria_9, Kat_9.Kat_9())
        self.setPage(self.Kategoria_10, Kat_10.Kat_10())
        self.setPage(self.StronaOstatnia, LastPage.LastPage())




        self.setStartId(self.PageIntro)


    def disable_back(self, ind):
        if ind in self.noback:
            self.button(QtGui.QWizard.BackButton).setEnabled(False)

    def enable_back(self, yesback, ind):
        if ind in yesback:
            self.button(QtGui.QWizard.BackButton).setEnabled(True)