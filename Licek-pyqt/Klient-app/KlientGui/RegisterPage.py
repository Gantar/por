# -*- coding: utf-8 -*-

import Frame

class RegisterPage(Frame.QtGui.QWizardPage):
    def __init__(self, parent=None):
        super(RegisterPage, self).__init__(parent)
        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setText(Frame.QtCore.QString.fromUtf8("Przegląd kategorii"))
        self.l1.setFont(Frame.QtGui.QFont("Times", 36))
        self.l1.move(300, 20)

        self.l2 = Frame.QtGui.QLabel(self)
        self.l2.setText(Frame.QtCore.QString.fromUtf8('''Plan opieki rodzicielskiej polega na porozumieniu obu stron w sprawie dotyczącej opieki 
nad dzieckiem, jednak plan opieki rodzicielskiej opiera sie na 10 głównych kategoriach, 
które zostają zaprezentowane poniżej. Do każdej z kategorii która Państwa interesjue proszę 
dodać opisową opcje, na którą później należy przydzielić punkty. O to główne kategorie: '''))
        self.l2.setFont(Frame.QtGui.QFont("Times", 20))
        self.l2.move(20, 80)



        self.kat1 = Frame.QtGui.QLabel(self)
        self.kat1.setText(
            Frame.QtCore.QString.fromUtf8("1. Kontakt z rodzicem - wychowanie i zajmowanie się dzieckiem"))
        self.kat1.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat1.move(50, 220)

        self.kat2 = Frame.QtGui.QLabel(self)
        self.kat2.setText(Frame.QtCore.QString.fromUtf8("2. Kontakt z pozostałymi członkami rodziny"))
        self.kat2.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat2.move(50, 250)

        self.kat3 = Frame.QtGui.QLabel(self)
        self.kat3.setText(Frame.QtCore.QString.fromUtf8("3. Rozwój dziecka"))
        self.kat3.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat3.move(50, 280)

        self.kat4 = Frame.QtGui.QLabel(self)
        self.kat4.setText(Frame.QtCore.QString.fromUtf8("4. Utrzymywanie dziecka"))
        self.kat4.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat4.move(50, 310)

        self.kat5 = Frame.QtGui.QLabel(self)
        self.kat5.setText(Frame.QtCore.QString.fromUtf8("5. Spędzanie wolnego czasu"))
        self.kat5.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat5.move(50, 340)

        self.kat6 = Frame.QtGui.QLabel(self)
        self.kat6.setText(Frame.QtCore.QString.fromUtf8("6. Światopogląd dziecka"))
        self.kat6.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat6.move(50, 370)

        self.kat7 = Frame.QtGui.QLabel(self)
        self.kat7.setText("7. Zdrowie dziecka")
        self.kat7.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat7.move(50, 400)

        self.kat8 = Frame.QtGui.QLabel(self)
        self.kat8.setText("8. Informowanie i dokumentacja")
        self.kat8.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat8.move(50, 430)

        self.kat9 = Frame.QtGui.QLabel(self)
        self.kat9.setText(Frame.QtCore.QString.fromUtf8("9. Postępowanie w nagłych przypadkach"))
        self.kat9.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat9.move(50, 460)

        self.kat10 = Frame.QtGui.QLabel(self)
        self.kat10.setText(Frame.QtCore.QString.fromUtf8("10. Kwestie pozostałe"))
        self.kat10.setFont(Frame.QtGui.QFont("Times", 20, Frame.QtGui.QFont.Bold))
        self.kat10.move(50, 490)


        def nextId(self):
            return self.DodajKategorie