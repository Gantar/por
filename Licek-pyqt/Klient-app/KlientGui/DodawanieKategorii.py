# -*- coding: utf-8 -*-
import Frame

class DodawanieKategorii(Frame.QtGui.QWizardPage):
    def __init__(self, parent=None):
        super(DodawanieKategorii, self).__init__(parent)
        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setText("Kreator kategorii")
        self.l1.setFont(Frame.QtGui.QFont("Times", 36))
        self.l1.move(300, 20)

        self.l2 = Frame.QtGui.QLabel(self)
        self.l2.setText(Frame.QtCore.QString.fromUtf8('''
Z poniżej rozwijanej listy proszę wybrać kategorie, następnie w polu tekstowym opisać opcję,
która nas interesuje. Do każdej kategorii można dodać nieskończoną liczbe opcji, 
jednak należy wziąć pod uwage to, iż algorytm wybierze opcję najbardziej optymalną, 
więc prosze o dodawanie opcji, które mogą zainteresować obie ze stron. '''))
        self.l2.setFont(Frame.QtGui.QFont("Times", 20))
        self.l2.move(0, 80)
        ###COMBO BOX ##########
        self.CB = Frame.QtGui.QComboBox(self)
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("1. Kontakt z rodzicem - wychowanie i zajmowanie się dzieckiem"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("2. Kontakt z pozostałymi członkami rodziny"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("3. Rozwój dziecka"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("4. Utrzymywanie dziecka"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("5. Spędzanie wolnego czasu"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("6. Światopogląd dziecka"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("7. Zdrowie dziecka"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("8. Informowanie i dokumentacja"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("9. Postępowanie w nagłych przypadkach"))
        self.CB.addItem(Frame.QtCore.QString.fromUtf8("10. Kwestie pozostałe"))
        self.CB.move(100, 350)

        ######ADD BUTTON##########

        self.ADDButton = Frame.QtGui.QPushButton(self)
        self.ADDButton.setText(Frame.QtCore.QString.fromUtf8("Dodaj kategorię"))
        self.connect(self.ADDButton, Frame.QtCore.SIGNAL("clicked()"), self.addCategory)
        self.ADDButton.move(650, 400)

        #####TEXT BOX ########

        self.textBox = Frame.QtGui.QLineEdit(self)
        self.textBox.move(100, 400)
        self.textBox.resize(400, 30)

        ############ Server List Object ##########
        #self.serverListObject = ServerList()

        ################ MESSAGE BOX ################

    def addDialog(self):
        self.msg = Frame.QtGui.QMessageBox(self)
        self.msg.setIcon(Frame.QtGui.QMessageBox.Question)
        self.msg.setStandardButtons(Frame.QtGui.QMessageBox.Yes | Frame.QtGui.QMessageBox.No)
        self.msg.setButtonText(Frame.QtGui.QMessageBox.Yes, "Tak")
        self.msg.setButtonText(Frame.QtGui.QMessageBox.No, "Nie")
        self.msg.setText(Frame.QtCore.QString.fromUtf8("Czy na pewno dodać tą kategorię?"))
        self.result = self.msg.exec_()



    def addCategory(self):
        self.addDialog()
        if self.result == Frame.QtGui.QMessageBox.Yes:
            if self.CB.currentText()[1] == "0":
                Frame.Frame.serverListObject.Kat_10_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "2":
                Frame.Frame.serverListObject.Kat_2_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "3":
                Frame.Frame.serverListObject.Kat_3_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "4":
                Frame.Frame.serverListObject.Kat_4_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "5":
                Frame.Frame.serverListObject.Kat_5_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "6":
                Frame.Frame.serverListObject.Kat_6_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "7":
                Frame.Frame.serverListObject.Kat_7_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "8":
                Frame.Frame.serverListObject.Kat_8_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "9":
                Frame.Frame.serverListObject.Kat_9_Option.append(str(self.textBox.text()))
            elif self.CB.currentText()[0] == "1":
                Frame.Frame.serverListObject.Kat_1_Option.append(str(self.textBox.text()))
            self.textBox.clear()
        else:
            Frame.Frame.serverListObject.printObject()



    def genKey(self):
        Frame.Frame.serverListObject.wygenerowanyKlucz = (
        Frame.Frame.serverListObject.kluczsprawy + "-" + Frame.Frame.serverListObject.nazwisko + "-" +
        Frame.Frame.serverListObject.imie + "-" + Frame.Frame.serverListObject.strona[:2])




    def nextId(self):
        self.genKey()
        return Frame.Frame.PolaczZSerwerem


