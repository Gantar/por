# -*- coding: utf-8 -*-
import Frame
import ConnectAndSend

class LastPage(Frame.QtGui.QWizardPage):
    def __init__(self, parent=None):
        super(LastPage, self).__init__(parent)
        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setText(Frame.QtCore.QString.fromUtf8(
'''Serdecznie dziękuje za skorzystanie z programu, gotowy formularz planu opieki rodzicielskiej 
zostanie wysłany do serwera, który znajdzie optymalne rozwiązanie dla problemu opieki.
Proszę poczekać, dopóki druga strona nie wypełni tego formularza, oraz proszę nie sugerować
się tym planem, program tylko pomaga w rozwiązaniu konfliktu, decyzję podejmuje sąd.'''))
        self.l1.setFont(Frame.QtGui.QFont("Times", 24))
        self.l1.move(20,20)

        self.checkButton = Frame.QtGui.QPushButton(self)
        self.checkButton.setText("Wyślij formularz!")
        self.checkButton.move(450, 450)
        self.checkButton.connect(self.checkButton, Frame.QtCore.SIGNAL("clicked()"), self.wyslij)

    def showMessage(self, answer):
        self.msg = Frame.QtGui.QMessageBox(self)
        self.msg.setIcon(Frame.QtGui.QMessageBox.Information)
        self.msg.setStandardButtons(Frame.QtGui.QMessageBox.Ok)
        self.msg.setText(Frame.QtCore.QString.fromUtf8(answer))
        self.result = self.msg.exec_()


    def wyslij(self):
        self.cas = ConnectAndSend.ConnectAndSend()
        self.cas.sending(Frame.Frame.readyObject)
        self.showMessage("Fomularz został wysłany!")
        self.pokazobiekt()

    def pokazobiekt(self):
        Frame.Frame.readyObject.printList()