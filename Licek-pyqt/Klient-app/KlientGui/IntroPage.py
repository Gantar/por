# -*- coding: utf-8 -*-
import Frame

class IntroPage(Frame.QtGui.QWizardPage):
    def __init__(self, parent=None):
        super(IntroPage, self).__init__(parent)
        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setGeometry(Frame.QtCore.QRect(60, 20, 871, 71))
        self.l1.setText("Serdecznie witam w kreatorze planu opieki rodzicielskiej!")
        self.l1.setFont(Frame.QtGui.QFont("Times", 24))
        self.l2 = Frame.QtGui.QLabel(self)
        self.l2.setGeometry(Frame.QtCore.QRect(100, 100, 871, 71))
        self.l2.setText(Frame.QtCore.QString.fromUtf8('''Program poprowadzi Cię przez kreator, którego zadaniem jest obliczenie optymalnego,
            planu opieki rodzicielskiej, który powinien usatysfakcjonować każdego z Was,
            proszę jednak dobrze przemyśleć swoje decyzje i podejmować je słusznie,'''))
        self.l3 = Frame.QtGui.QLabel(self)
        self.l3.setText(Frame.QtCore.QString.fromUtf8("biorąc pod uwage jedynie dobro dziecka!"))
        self.l3.move(220,180)
        self.l3.setFont(Frame.QtGui.QFont("Helvetica", 16))

        self.l2.setFont(Frame.QtGui.QFont("Helvetica", 16))
        self.l3.setFont(Frame.QtGui.QFont("Helvetica", 20))
   # def nextId(self):
           # return Frame.PageEvaluate