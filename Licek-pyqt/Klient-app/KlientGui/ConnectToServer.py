# -*- coding: utf-8 -*-
import Frame, ConnectAndSend

class ConnectToServer(Frame.QtGui.QWizardPage, ConnectAndSend.ConnectAndSend):
    def __init__(self, parent=None):
        super(ConnectToServer, self).__init__(parent)
        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setText(Frame.QtCore.QString.fromUtf8("Realizacja"))
        self.l1.setFont(Frame.QtGui.QFont("Times", 36))
        self.l1.move(400, 20)

        self.l2 = Frame.QtGui.QLabel(self)
        self.l2.setText(Frame.QtCore.QString.fromUtf8('''
Poniżej zostanie wygenerowany klucz, dzięki któremu zostanie pobrany główny formularz 
planu opieki rodziecielskiej. Formularz zostanie wygenerowany, wtedy gdy druga strona 
również zakończy kreator. Proszę wygenerować i zapisać niżej podany kod.'''))
        self.l2.setFont(Frame.QtGui.QFont("Times", 20))
        self.l2.move(20, 100)

        self.textBox = Frame.QtGui.QLineEdit(self)
        self.textBox.setEnabled(False)
        self.textBox.move(300, 400)
        self.textBox.resize(200, 30)

        self.genKeyButton = Frame.QtGui.QPushButton(self)
        self.genKeyButton.setText("Generuj klucz")
        self.genKeyButton.move(350, 450)
        self.connect(self.genKeyButton, Frame.QtCore.SIGNAL("clicked()"), self.generateKey)


        self.ClipboardButton = Frame.QtGui.QPushButton(self)
        self.ClipboardButton.setText("Kopiuj do schowka")
        self.ClipboardButton.move(600, 400)
        self.connect(self.ClipboardButton, Frame.QtCore.SIGNAL("clicked()"), self.copyToClipboard)



    def copyToClipboard(self):
        self.clipBoard = Frame.QtGui.QApplication.clipboard()
        self.clipBoard.setText(self.textBox.text(), mode=self.clipBoard.Clipboard)

    def generateKey(self):
        self.textBox.setText(Frame.Frame.serverListObject.wygenerowanyKlucz)
        self.cas = ConnectAndSend.ConnectAndSend()
        self.cas.sending(Frame.Frame.serverListObject)
        self.nextId()





    def nextId(self):
        return Frame.Frame.WyslijKlucz