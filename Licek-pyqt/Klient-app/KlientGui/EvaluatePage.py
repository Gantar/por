# -*- coding: utf-8 -*-
import Frame

class EvaluatePage(Frame.QtGui.QWizardPage):
    def __init__(self, parent=None):
        super(EvaluatePage, self).__init__(parent)

        self.l1 = Frame.QtGui.QLabel(self)
        self.l1.setText("Plan opieki rodzicielskiej")
        self.l1.setFont(Frame.QtGui.QFont("Times", 36))
        self.l1.move(250,20)
        self.limie = Frame.QtGui.QLabel(self)
        self.limie.setText(Frame.QtCore.QString.fromUtf8("Imię: "))
        self.limie.setFont(Frame.QtGui.QFont("Times", 20))
        self.limie.move(50, 100)
        self.tbimie = Frame.QtGui.QLineEdit(self)
        self.tbimie.move(130, 105)
        self.lnazwisko = Frame.QtGui.QLabel(self)
        self.lnazwisko.setText("Nazwisko: ")
        self.lnazwisko.setFont(Frame.QtGui.QFont("Times", 20))
        self.lnazwisko.move(350, 100)
        self.tbnazwisko = Frame.QtGui.QLineEdit(self)
        self.tbnazwisko.move(500, 105)
        self.lkluczsprawy = Frame.QtGui.QLabel(self)
        self.lkluczsprawy.setText("Klucz: ")
        self.lkluczsprawy.setFont(Frame.QtGui.QFont("Times", 20))
        self.lkluczsprawy.move(700,100)
        self.tbkluczsprawy = Frame.QtGui.QLineEdit(self)
        self.tbkluczsprawy.move(800, 105)
        ###STRONA#####
        self.lstrona = Frame.QtGui.QLabel(self)
        self.lstrona.setText("Strona: ")
        self.lstrona.setFont(Frame.QtGui.QFont("Times", 20))
        self.lstrona.move(20, 150)
        self.RBOjciec = Frame.QtGui.QRadioButton(self)
        self.RBOjciec.setText("Ojciec")
        self.RBOjciec.setFont(Frame.QtGui.QFont("Times", 20))
        self.RBOjciec.move(150, 150)
        self.RBOjciec.toggled.connect(lambda :self.RBValue(self.RBOjciec))
        self.RBMatka = Frame.QtGui.QRadioButton(self)
        self.RBMatka.setText("Matka")
        self.RBMatka.setFont(Frame.QtGui.QFont("Times", 20))
        self.RBMatka.move(300, 150)
        self.RBMatka.toggled.connect(lambda: self.RBValue(self.RBMatka))
        self.RBMediator = Frame.QtGui.QRadioButton(self)
        self.RBMediator.setText("Mediator")
        self.RBMediator.setFont(Frame.QtGui.QFont("Times", 20))
        self.RBMediator.move(450, 150)
        self.RBMediator.toggled.connect(lambda: self.RBValue(self.RBMediator))
        self.checkBox = ""

        ######TEKST##########

        self.Linfo = Frame.QtGui.QLabel(self)
        self.Linfo.setText(Frame.QtCore.QString.fromUtf8('''Z podanych danych zostanie wygenerowany klucz planu opieki rodzicielskiej,
który będzie potrzebny do dalszego tworzenia planu opieki rodzicielskiej.
Unikalny klucz stworzy relacje, dzięki której będzie możliwe wybranie najlepszej strategii'''))
        self.Linfo.setFont(Frame.QtGui.QFont("Times", 20))
        self.Linfo.move(20, 200)

        ##### WHEN I HAVE A KEY ########

        self.LHaveKey = Frame.QtGui.QLabel(self)
        self.LHaveKey.setText(Frame.QtCore.QString.fromUtf8('''
Jeżeli posiadasz już klucz, zaznacz opcje "Mam już klucz! ", aby przejść
do pobrania formularza'''))
        self.LHaveKey.setFont(Frame.QtGui.QFont("Times", 20))
        self.LHaveKey.move(20, 300)

        self.CBHaveKey = Frame.QtGui.QCheckBox(self)
        self.CBHaveKey.setText(Frame.QtCore.QString.fromUtf8("Mam już klucz!"))
        self.CBHaveKey.setFont(Frame.QtGui.QFont("Times", 18))
        self.CBHaveKey.move(200, 450)
        self.CBresutl =""


        ######KLUCZ-LABEL############



        ######BUTTON-KLUCZ#######

        #self.Kbutton = QtGui.QPushButton(self)
        #self.Kbutton.setText("Akceptuj")
        #self.Kbutton.setObjectName("akceptuj")
        #self.connect(self.Kbutton, QtCore.SIGNAL("clicked()"), self.getKlucz)
        #self.Kbutton.move(425, 450)




    def RBValue(self, b):
       self.checkBox = b.text()




    def getKlucz(self):
        self.imie = self.tbimie.text()
        self.nazwisko = self.tbnazwisko.text()
        self.klucz = self.imie + self.nazwisko + self.checkBox
        #self.KLabel.setText(self.klucz)
        Frame.Frame.serverListObject.imie = self.imie
        Frame.Frame.serverListObject.nazwisko = self.nazwisko
        Frame.Frame.serverListObject.strona = self.checkBox
        Frame.Frame.serverListObject.kluczsprawy = self.tbkluczsprawy.text()





    def nextId(self):
        if self.CBHaveKey.isChecked():
            return Frame.Frame.WyslijKlucz
        else:
            self.getKlucz()
            return Frame.Frame.PageRegister