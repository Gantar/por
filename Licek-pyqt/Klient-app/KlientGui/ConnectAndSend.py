# -*- coding: utf-8 -*-
import socket
import cPickle as pickle

class ConnectAndSend(object):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(("127.0.01", 2094))



    def sending(self, object):

        self.sock.sendall(self.pack(object) + "\r\n")


    def reciving(self):
        print(self.recv_all_until(self.sock, "\r\n"))

    def pack(self, obj):
        self.pick = pickle.dumps(obj)
        return self.pick

    def sendRequest(self, data):   #### Wyślij zapytanie o formularz
        self.sock.sendall(data + "\r\n")
        answer = self.recv_all_until(self.sock, "\r\n")
        print answer
        return answer

    def recv_all_until(self, sockfd, crlf):
        data = ""
        while not data.endswith(crlf):
            data = data + sockfd.recv(1)
        return data
