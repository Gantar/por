# -*- coding: utf-8 -*-
import sys
sys.path.append("..")
import socket

from Server import Base
from Server import ReadyForm
from package import Serialization


class Server(object):
    def __init__(self):
        self.data_loadet = Serialization.Serialization()
        self.baseObject = Base.Base()
        self.readyFormObject = ReadyForm.ReadyForm()

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_addres = ("127.0.01", 2094)
        self.sock.bind(self.server_addres)
        self.request = ""
        print 'Starting up TCP server on %s port %s' % self.server_addres
        while True:
            self.sock.listen(1)

            self.connection, self.client_address = self.sock.accept()

            print "Client %s connected ..." % str(self.client_address)

            try:
                self.data = self.recv_all_until(self.connection, "\r\n")
                if self.data:
                    serializationObject = Serialization.Serialization()
                    if serializationObject.is_pickle(self.data):
                        print " Obiekt serializowany"

                        self.data_loadet = serializationObject.un_pack(self.data)

                        if self.baseObject.check_list_is_ready_for_form(self.data_loadet):
                            self.baseObject.pushOnRFL(self.data_loadet)
                            self.baseObject.checkRFLflags()

                        else:
                            self.baseObject.checkList(self.data_loadet)
                            print "Lista kategorii: \n"
                            self.baseObject.printList()
                    else:
                        print "zapytanie"
                        self.request = self.data.strip() ### When the key to get form
                        #self.get_key(self.request)
                        print self.request
                        self.send_ready_list()
                    self.baseObject.printDict()











            finally:
                self.connection.close()

    def get_key(self, data):
        req = data
        req.split('-')

        imie = req[1]
        nazwisko = req[2]
        klucz = req[0]
        strona = req[3]
        print "klucz to : " +str(klucz)
        return klucz

    def send_ready_list(self):
        if self.baseObject.check_list_is_ready(self.request):
            serializationObject = Serialization.Serialization()
            print self.baseObject.get_ready_list(self.request)
            print serializationObject.pack(self.baseObject.get_ready_list(self.request))
            self.connection.sendall(serializationObject.pack(self.baseObject.get_ready_list(self.request))+ "\r\n")
        else:
            self.connection.sendall("Formularz nie jest gotowy, poczekaj! \r\n")

    def recv_all_until(self, sockfd, crlf):
        data = ""
        while not data.endswith(crlf):
            data = data + sockfd.recv(1)
        return data





def main():
    server = Server()

main()

