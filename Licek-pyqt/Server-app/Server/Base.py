# -*- coding: utf-8 -*-
import Algorithm
import FinalObject
import ReadyForm
import ServerListObject


class Base(object):
    def __init__(self):
        self.slo = ServerListObject.ServerListObject()
        self.objectList = []
        self.readyFormList = []
        self.readyFormObject = ReadyForm.ReadyForm()
        self.finalObject = FinalObject.FinalObject()
        self.finalObjectList = []
        self.algo = Algorithm.Algorithm()


    def checkList(self, data):
        if not self.objectList:
            self.slo = self.slo.create(data)
            self.pushOnList(self.slo)
        else:
            for i in self.objectList:
                if self.slo.dict["kluczsprawy"] == data.kluczsprawy:
                   if data.strona == "Ojciec":
                       if self.slo.dict["ojciec"] == 1:
                           print "Object exist"
                       else:
                           self.slo.extendList(self.slo,data)
                           self.slo.dict["ojciec"] = 1
                   elif data.strona == "Matka":
                       if self.slo.dict["matka"] == 1:
                           print "Object exist"
                       else:
                           self.slo.extendList(self.slo,data)
                           self.slo.dict["matka"] = 1
                   elif data.strona == "Mediator":
                       if self.slo.dict["mediator"] == 1:
                           print "Object exist"
                       else:
                           self.slo.extendList(self.slo,data)
                           self.slo.dict["mediator"] = 1

                else:
                    self.slo = self.slo.create(data)
                    self.pushOnList(self.slo)





    def pushOnList(self, data):
        self.objectList.append(data)



    def printList(self):
        for i in range(len(self.objectList)):
            print self.objectList[i].dict
            print self.objectList[i].sl.printObject()

    def printDict(self):
        for i in range(len(self.objectList)):
            print self.objectList[i].dict




    def check_list_is_ready_for_form(self, data):
        for i in range(len(self.objectList)):
            if self.objectList[i].dict["kluczsprawy"] == data.kluczsprawy:
                if self.objectList[i].dict["ojciec"] == 1\
                        and self.objectList[i].dict["matka"] == 1 and self.objectList[i].dict["mediator"] == 1:
                    return True


    def check_list_is_ready(self, data):
        for i in range(len(self.objectList)):
            if self.objectList[i].dict["kluczsprawy"] == data:
                if self.objectList[i].dict["ojciec"] == 1\
                        and self.objectList[i].dict["matka"] == 1 and  self.objectList[i].dict["mediator"] == 1:
                    return True
                else:
                    print "Brak gotowosci"
                    return False

    def get_ready_list(self, data):
        for i in range(len(self.objectList)):
            if self.objectList[i].dict["kluczsprawy"] == data:
                return self.objectList[i].sl


    ############FUNCJE DO READYFORM ############

    def pushOnRFL(self, data):
        if not self.readyFormList:
            self.createObjectRFL(data)
            self.readyFormList.append(self.readyFormObject)
        else:
            for i in range(len(self.readyFormList)):
                if self.readyFormList[i].kluczsprawy == data.kluczsprawy:
                    if data.strona == "Ma":
                        if self.readyFormList[i].matka_flaga == 1:
                            print "Obiekt matki juz istnieje"
                        else:
                            self.readyFormList[i].matka = data
                            self.readyFormList[i].matka_flaga = 1
                    elif data.strona == "Oj":
                        if self.readyFormList[i].ojciec_flaga == 1:
                            print "Obiekt ojca juz istnieje"
                        else:
                            self.readyFormList[i].ojciec = data
                            self.readyFormList[i].ojciec_flaga = 1
                else:
                    self.createObjectRFL(data)
                    self.readyFormList.append(self.readyFormObject)
        self.checkReady()






    def checkReady(self):
        for i in range(len(self.readyFormList)):
            if self.readyFormList[i].matka_flaga == 1 and self.readyFormList[i].ojciec_flaga == 1:
                print "Sprawa z kluczem : " +self.readyFormList[i].kluczsprawy + " jest gotowa!"





    def createObjectRFL(self, data):
        self.readyFormObject.kluczsprawy = data.kluczsprawy
        if data.strona == "Oj":
            self.readyFormObject.ojciec_flaga = 1
            self.readyFormObject.ojciec = data
        elif data.strona == "Ma":
            self.readyFormObject.matka_flaga = 1
            self.readyFormObject.matka = data
        print "Obiekt : " + data.strona+ " utworzony!"


    def printRFL(self):
        for i in range(len(self.readyFormList)):
            self.readyFormList[i].wyswietl()

    def checkRFLflags(self):
        for i in range(len(self.readyFormList)):
           if self.readyFormList[i].matka_flaga == 1 and self.readyFormList[i].ojciec_flaga == 1:
               print "Lista slownikow gotowa"
               for j in range(len(self.objectList)):
                   if self.objectList[j].dict["kluczsprawy"] == self.readyFormList[i].kluczsprawy:
                       self.finalObject.createDict(self.objectList[j], self.readyFormList[i])
                       self.finalObjectList.append(self.finalObject)

                       self.finalObject.printFinalObject()
                       self.algo.getBestPath(self.finalObject)


           else:
               print "Lista slownikow Niegotowa!"
















