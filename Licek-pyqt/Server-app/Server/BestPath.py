# -*- coding: utf-8 -*-

class BestPath(object):
    def __init__(self):
        self.dict_1 = {}
        self.dict_2 = {}
        self.dict_3 = {}
        self.dict_4 = {}
        self.dict_5 = {}
        self.dict_6 = {}
        self.dict_7 = {}
        self.dict_8 = {}
        self.dict_9 = {}
        self.dict_10 = {}
        self.suma_punktacji_ojca = 0
        self.suma_punktacji_matki = 0
        self.wspolna_suma = 0

    def countSum(self):
        suma_punktacji_ojca = 0
        suma_punktacji_matki = 0
        wspolna_suma = 0
        suma_punktacji_ojca += self.dict_1["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_1["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_2["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_2["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_3["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_3["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_4["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_4["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_5["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_5["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_6["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_6["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_7["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_7["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_8["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_8["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_9["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_9["Punktacja Matki"]
        suma_punktacji_ojca += self.dict_10["Punktacja Ojca"]
        suma_punktacji_matki += self.dict_10["Punktacja Matki"]
        wspolna_suma = suma_punktacji_matki + suma_punktacji_ojca

        ##Przypisanie

        self.suma_punktacji_matki = suma_punktacji_matki
        self.suma_punktacji_ojca = suma_punktacji_ojca
        self.wspolna_suma = wspolna_suma



    def showBestPath(self):
        print self.dict_1
        print self.dict_2
        print self.dict_3
        print self.dict_4
        print self.dict_5
        print self.dict_6
        print self.dict_7
        print self.dict_8
        print self.dict_9
        print self.dict_10

        print "Ojciec: " +str(self.suma_punktacji_ojca)
        print "Matka: " +str(self.suma_punktacji_matki)
        print "Wspolna suma: " +str(self.wspolna_suma)

    def konkatenacjaSum(self):
        print "(" + str(self.suma_punktacji_matki) + " , " + str(self.suma_punktacji_ojca) + ")"

