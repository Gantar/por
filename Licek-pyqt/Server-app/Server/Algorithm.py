# -*- coding: utf-8 -*-
import Base, BestPath, copy

class Algorithm(object):
    def __init__(self):
        self.bestPath = BestPath.BestPath()
        self.tempbestPath = BestPath.BestPath()
        self.sumaOjcaTemp = 0
        self.sumaMatkiTemp = 0
        self.najlepszaSumaMatki = 0
        self.najlepszaSumaOjca = 0
        self.roznica = 0
        self.listaproba = []
        self.pathList = []



    # def getBestPath(self,data):
    #     for i1 in range(len(data.kat_1)):
    #         self.tempbestPath.dict_1 = data.kat_1[i1]
    #         self.sumaOjcaTemp += data.kat_1[i1]["Punktacja Ojca"]
    #         self.sumaMatkiTemp += data.kat_1[i1]["Punktacja Matki"]
    #         for i2 in range(len(data.kat_2)):
    #             self.tempbestPath.dict_2 = data.kat_2[i2]
    #             self.sumaOjcaTemp += data.kat_2[i2]["Punktacja Ojca"]
    #             self.sumaMatkiTemp += data.kat_2[i2]["Punktacja Matki"]
    #             for i3 in range(len(data.kat_3)):
    #                 self.tempbestPath.dict_3 = data.kat_3[i3]
    #                 self.sumaOjcaTemp += data.kat_3[i3]["Punktacja Ojca"]
    #                 self.sumaMatkiTemp += data.kat_3[i3]["Punktacja Matki"]
    #                 for i4 in range(len(data.kat_4)):
    #                     self.tempbestPath.dict_4 = data.kat_4[i4]
    #                     self.sumaOjcaTemp += data.kat_4[i4]["Punktacja Ojca"]
    #                     self.sumaMatkiTemp += data.kat_4[i4]["Punktacja Matki"]
    #                     for i5 in range(len(data.kat_5)):
    #                         self.tempbestPath.dict_5 = data.kat_5[i5]
    #                         self.sumaOjcaTemp += data.kat_5[i5]["Punktacja Ojca"]
    #                         self.sumaMatkiTemp += data.kat_5[i5]["Punktacja Matki"]
    #                         for i6 in range(len(data.kat_6)):
    #                             self.tempbestPath.dict_6 = data.kat_6[i6]
    #                             self.sumaOjcaTemp += data.kat_6[i6]["Punktacja Ojca"]
    #                             self.sumaMatkiTemp += data.kat_6[i6]["Punktacja Matki"]
    #                             for i7 in range(len(data.kat_7)):
    #                                 self.tempbestPath.dict_7 = data.kat_7[i7]
    #                                 self.sumaOjcaTemp += data.kat_7[i7]["Punktacja Ojca"]
    #                                 self.sumaMatkiTemp += data.kat_7[i7]["Punktacja Matki"]
    #                                 for i8 in range(len(data.kat_8)):
    #                                     self.tempbestPath.dict_8 = data.kat_8[i8]
    #                                     self.sumaOjcaTemp += data.kat_8[i8]["Punktacja Ojca"]
    #                                     self.sumaMatkiTemp += data.kat_8[i8]["Punktacja Matki"]
    #                                     for i9 in range(len(data.kat_9)):
    #                                         self.tempbestPath.dict_9 = data.kat_9[i9]
    #                                         self.sumaOjcaTemp += data.kat_9[i9]["Punktacja Ojca"]
    #                                         self.sumaMatkiTemp += data.kat_9[i9]["Punktacja Matki"]
    #                                         for i10 in range(len(data.kat_10)):
    #                                             self.tempbestPath.dict_10 = data.kat_10[i10]
    #                                             self.sumaOjcaTemp += data.kat_10[i10]["Punktacja Ojca"]
    #                                             self.sumaMatkiTemp += data.kat_10[i10]["Punktacja Matki"]
    #                                             ##Sprawdz czy punktacja jest lepsza od poprzedniej
    #
    #                                             if self.sumaMatkiTemp + self.sumaOjcaTemp > self.bestScore:
    #                                                 self.bestScore = self.sumaOjcaTemp + self.sumaMatkiTemp
    #                                                 self.bestPath = self.tempbestPath
    #                                             self.sumaMatkiTemp = 0
    #                                             self.sumaOjcaTemp = 0

    def getBestPath(self, data):
        self.createList(data)
        self.biggestSum(data)
        #self.min_max_Matka(data)
        #self.min_max_Ojciec(data)
        self.min_max_Matka_2(data)
        self.min_max_Ojciec_2(data)

    def createList(self, data): ##Konkatenacja wszystkich sciezek
        tempbestPath = BestPath.BestPath()
        for i1 in range(len(data.kat_1)):
            tempbestPath.dict_1 = data.kat_1[i1]
            for i2 in range(len(data.kat_2)):
                tempbestPath.dict_2 = data.kat_2[i2]
                for i3 in range(len(data.kat_3)):
                    tempbestPath.dict_3 = data.kat_3[i3]
                    for i4 in range(len(data.kat_4)):
                        tempbestPath.dict_4 = data.kat_4[i4]
                        for i5 in range(len(data.kat_5)):
                            tempbestPath.dict_5 = data.kat_5[i5]
                            for i6 in range(len(data.kat_6)):
                                tempbestPath.dict_6 = data.kat_6[i6]
                                for i7 in range(len(data.kat_7)):
                                    tempbestPath.dict_7 = data.kat_7[i7]
                                    for i8 in range(len(data.kat_8)):
                                        tempbestPath.dict_8 = data.kat_8[i8]
                                        for i9 in range(len(data.kat_9)):
                                            tempbestPath.dict_9 = data.kat_9[i9]
                                            for i10 in range(len(data.kat_10)):
                                                tempbestPath.dict_10 = data.kat_10[i10]
                                                tempbestPath.countSum()
                                                self.bestPath = copy.deepcopy(tempbestPath)
                                                self.pathList.append(self.bestPath)




    def biggestSum(self, data):
         bestScore = BestPath.BestPath()
         bestScore.wspolna_suma = 0
         for i in range(len(self.pathList)):
            if(self.pathList[i].wspolna_suma > bestScore.wspolna_suma):
                bestScore.dict_1 = self.pathList[i].dict_1
                bestScore.dict_2 = self.pathList[i].dict_2
                bestScore.dict_3 = self.pathList[i].dict_3
                bestScore.dict_4 = self.pathList[i].dict_4
                bestScore.dict_5 = self.pathList[i].dict_5
                bestScore.dict_6 = self.pathList[i].dict_6
                bestScore.dict_7 = self.pathList[i].dict_7
                bestScore.dict_8 = self.pathList[i].dict_8
                bestScore.dict_9 = self.pathList[i].dict_9
                bestScore.dict_10 = self.pathList[i].dict_10
                bestScore.suma_punktacji_matki = self.pathList[i].suma_punktacji_matki
                bestScore.suma_punktacji_ojca = self.pathList[i].suma_punktacji_ojca
                bestScore.wspolna_suma = self.pathList[i].wspolna_suma

         print "Najwieksza wspolna suma: "
         bestScore.showBestPath()
         with open(data.kluczsprawy, 'a' ) as f:
             f.write("Najwieksza wspolna suma \n")
             f.write(str(bestScore.dict_1) + "\n")
             f.write(str(bestScore.dict_2)+ "\n")
             f.write(str(bestScore.dict_3)+ "\n")
             f.write(str(bestScore.dict_4)+ "\n")
             f.write(str(bestScore.dict_5)+ "\n")
             f.write(str(bestScore.dict_6)+ "\n")
             f.write(str(bestScore.dict_7)+ "\n")
             f.write(str(bestScore.dict_8)+ "\n")
             f.write(str(bestScore.dict_9)+ "\n")
             f.write(str(bestScore.dict_10)+ "\n")
             f.write("suma punktacji ojca: " + str(bestScore.suma_punktacji_ojca)+ "\n")
             f.write("suma punktacji matki: " + str(bestScore.suma_punktacji_matki)+ "\n")
             f.write("najwieksza wspolna suma: " + str(bestScore.wspolna_suma)+ "\n")





    def min_max_Matka_2(self, data):


        print "Min-Max Matka 2.0"
        tmpPathList = self.pathList

        #for i in range(len(tmpPathList)):
            #tmpPathList[i].showBestPath()

        bestPathList = []
        bestPathList1 = []
        maxBestPath = BestPath.BestPath()
        tmpBestPath = BestPath.BestPath()
        for i in range(0, len(tmpPathList), len(data.kat_10)):
            for x in range(len(data.kat_10)):
                if(maxBestPath.suma_punktacji_matki < tmpPathList[i+x].suma_punktacji_matki):
                    maxBestPath = tmpPathList[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        for i in range(0, len(bestPathList), len(data.kat_9)):
            for x in range(len(data.kat_9)):
                if(maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_8)):
            for x in range(len(data.kat_8)):
                if(maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(0, len(bestPathList), len(data.kat_7)):
            for x in range(len(data.kat_7)):
                if(maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_6)):
            for x in range(len(data.kat_6)):
                if(maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(0, len(bestPathList), len(data.kat_5)):
            for x in range(len(data.kat_5)):
                if(maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_4)):
            for x in range(len(data.kat_4)):
                if(maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(0, len(bestPathList), len(data.kat_3)):
            for x in range(len(data.kat_3)):
                if(maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_2)):
            for x in range(len(data.kat_2)):
                if(maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(len(bestPathList)):
            if(maxBestPath.suma_punktacji_ojca < bestPathList[i].suma_punktacji_ojca):
                maxBestPath = bestPathList[i]
        maxBestPath.showBestPath()

        with open(data.kluczsprawy, 'a') as f:
            f.write("Min-Max dla Matki \n")
            f.write(str(maxBestPath.dict_1) + "\n")
            f.write(str(maxBestPath.dict_2) + "\n")
            f.write(str(maxBestPath.dict_3) + "\n")
            f.write(str(maxBestPath.dict_4) + "\n")
            f.write(str(maxBestPath.dict_5) + "\n")
            f.write(str(maxBestPath.dict_6) + "\n")
            f.write(str(maxBestPath.dict_7) + "\n")
            f.write(str(maxBestPath.dict_8) + "\n")
            f.write(str(maxBestPath.dict_9) + "\n")
            f.write(str(maxBestPath.dict_10) + "\n")
            f.write("suma punktacji ojca: " + str(maxBestPath.suma_punktacji_ojca) + "\n")
            f.write("suma punktacji matki: " + str(maxBestPath.suma_punktacji_matki) + "\n")
            f.write("najwieksza wspolna suma: " + str(maxBestPath.wspolna_suma) + "\n")


    def min_max_Ojciec_2(self, data):
        print "Min-Max Ojciec 2.0"
        tmpPathList = self.pathList
        bestPathList = []
        bestPathList1 = []
        maxBestPath = BestPath.BestPath()
        tmpBestPath = BestPath.BestPath()

        for i in range(0, len(tmpPathList), len(data.kat_10)):
            for x in range(len(data.kat_10)):
                if (maxBestPath.suma_punktacji_ojca < tmpPathList[i + x].suma_punktacji_ojca):
                    maxBestPath = tmpPathList[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        for i in range(0, len(bestPathList), len(data.kat_9)):
            for x in range(len(data.kat_9)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_8)):
            for x in range(len(data.kat_8)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList1[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(0, len(bestPathList), len(data.kat_7)):
            for x in range(len(data.kat_7)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_6)):
            for x in range(len(data.kat_6)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList1[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(0, len(bestPathList), len(data.kat_5)):
            for x in range(len(data.kat_5)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_4)):
            for x in range(len(data.kat_4)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList1[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(0, len(bestPathList), len(data.kat_3)):
            for x in range(len(data.kat_3)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList = []
        for i in range(0, len(bestPathList1), len(data.kat_2)):
            for x in range(len(data.kat_2)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList1[i + x]
            tmpBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpBestPath)
            maxBestPath = BestPath.BestPath()
        bestPathList1 = []
        for i in range(len(bestPathList)):
            if (maxBestPath.suma_punktacji_matki < bestPathList[i].suma_punktacji_matki):
                maxBestPath = bestPathList[i]

        maxBestPath.showBestPath()

        with open(data.kluczsprawy, 'a') as f:
            f.write("Min-Max dla Ojca \n")
            f.write(str(maxBestPath.dict_1) + "\n")
            f.write(str(maxBestPath.dict_2) + "\n")
            f.write(str(maxBestPath.dict_3) + "\n")
            f.write(str(maxBestPath.dict_4) + "\n")
            f.write(str(maxBestPath.dict_5) + "\n")
            f.write(str(maxBestPath.dict_6) + "\n")
            f.write(str(maxBestPath.dict_7) + "\n")
            f.write(str(maxBestPath.dict_8) + "\n")
            f.write(str(maxBestPath.dict_9) + "\n")
            f.write(str(maxBestPath.dict_10) + "\n")
            f.write("suma punktacji ojca: " + str(maxBestPath.suma_punktacji_ojca) + "\n")
            f.write("suma punktacji matki: " + str(maxBestPath.suma_punktacji_matki) + "\n")
            f.write("najwieksza wspolna suma: " + str(maxBestPath.wspolna_suma) + "\n")

    def min_max_Matka(self, data):
        tmpPathList = self.pathList
        bestPathList = []
        bestPathList1 = []
        maxBestPath = BestPath.BestPath()
        tmpMaxBestPath = BestPath.BestPath()
        i, x = 0, 0
        print "lista dla kat 10: "
        for i in range(len(tmpPathList)):
            tmpPathList[i].konkatenacjaSum()
        while i < (len(tmpPathList)):
            x = 0
            while x < (len(data.kat_10)):
                if(maxBestPath.suma_punktacji_matki < tmpPathList[i+x].suma_punktacji_matki):
                    maxBestPath = tmpPathList[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0

        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_9)):
                if(maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_8)):
                if (maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList1 = []
        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_7)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_6)):
                if (maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList1 = []
        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_5)):

                if (maxBestPath.suma_punktacji_ojca < bestPathList[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_4)):
                if (maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList1 = []
        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_3)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList1.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_2)):
                if (maxBestPath.suma_punktacji_matki < bestPathList1[i+x].suma_punktacji_matki):
                    maxBestPath = bestPathList1[i+x]
                x += 1
            i += x
            tmpMaxBestPath = copy.deepcopy(maxBestPath)
            bestPathList.append(tmpMaxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        i, x = 0, 0
        print " Lista dla kat 2 "
        for i in range(len(bestPathList1)):
            bestPathList1[i].konkatenacjaSum()
        print "koniec listy dla kat 2 "
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_1)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList[i+x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i+x]
                x += 1
            i += x
        print "min-max dla matki: "
        maxBestPath.showBestPath()




        #### Pokaz najlepsza sciezke wyborow ####
       # self.bestPath.showBestPath()

    def min_max_Ojciec(self, data):
        tmpPathList = self.pathList
        bestPathList = []
        bestPathList1 = []
        maxBestPath = BestPath.BestPath()
        i, x = 0, 0
        while i < (len(tmpPathList)):
            x = 0
            while x < (len(data.kat_10)):
                if (maxBestPath.suma_punktacji_ojca < tmpPathList[i + x].suma_punktacji_ojca):
                    maxBestPath = tmpPathList[i + x]
                x += 1
            i += x
            bestPathList.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0

        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_9)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList1.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_8)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList1 = []
        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_7)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList1.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_6)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList1 = []
        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_5)):

                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList1.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_4)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList1 = []
        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_3)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList1.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList = []
        i, x = 0, 0
        while i < (len(bestPathList1)):
            x = 0
            while x < (len(data.kat_2)):
                if (maxBestPath.suma_punktacji_ojca < bestPathList1[i + x].suma_punktacji_ojca):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
            bestPathList.append(maxBestPath)
            maxBestPath.suma_punktacji_matki = 0
            maxBestPath.suma_punktacji_ojca = 0
        bestPathList1 = []
        i, x = 0, 0
        while i < (len(bestPathList)):
            x = 0
            while x < (len(data.kat_1)):
                if (maxBestPath.suma_punktacji_matki < bestPathList[i + x].suma_punktacji_matki):
                    maxBestPath = bestPathList[i + x]
                x += 1
            i += x
        print "min-max dla Ojca: "
        maxBestPath.showBestPath()












