# -*- coding: utf-8 -*-
import cPickle as pickle

import ServerList


class Serialization(object):
    def __init__(self):
        self.data_loaded = ServerList.ServerList()


    def un_pack(self, data):
        self.data_loaded = pickle.loads(data)
        return self.data_loaded

    def is_pickle(self, data):
        try:
            pickle.loads(data)
            return True
        except:
            return False


    def pack(self, obj):
        self.pick = pickle.dumps(obj)
        return self.pick


